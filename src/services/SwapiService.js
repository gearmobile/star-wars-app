export default class SwapiService {
  _basicURL = `https://swapi.co/api`;

  async getData(url = '') {
    const response = await fetch(`${this._basicURL}${url}`);
    if (!response.ok) {
      throw new Error(
        `Could not fetch ${this._basicURL}${url} received ${response.status}`
      );
    }
    return await response.json();
  }

  getAllPeople() {
    return this.getData(`/people/`);
  }

  getPerson(id) {
    return this.getData(`/people/${id}`);
  }

  getAllStarShips() {
    return this.getData(`/starships/`);
  }

  getStarShip(id) {
    return this.getData(`/starships/${id}`);
  }

  getAllPlanets() {
    return this.getData(`/planets/`);
  }

  getPlanet(id) {
    return this.getData(`/planets/${id}`);
  }
}
