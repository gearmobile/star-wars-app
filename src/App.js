import React, { Component } from 'react';
import { Header as AppHeader } from './components/Header/Header';
import { Preview as AppPreview } from './components/Preview/Preview';
import { List as AppList } from './components/List/List';
import { Details as AppDetails } from './components/Details/Details';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col">
              <AppHeader />
            </div>
          </div>
          <div className="row">
            <div className="col">
              <AppPreview />
            </div>
          </div>
          <div className="row">
            <div className="col-5">
              <AppList />
            </div>
            <div className="col-7">
              <AppDetails />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
