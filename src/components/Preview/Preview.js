import React, { Component } from 'react';
import { random } from 'lodash';
import SwapiService from '../../services/SwapiService';
// service.getAllPlanets().then(planets => console.log(planets.count));

export class Preview extends Component {
  service = new SwapiService();

  constructor() {
    super();
    this.updatePlanet();
  }

  state = {
    title: null,
    population: null,
    rotation: null,
    diameter: null,
    qty: null
  };

  updatePlanet() {
    this.service.getPlanet(random(61)).then(planet => {
      this.setState({
        title: planet.name,
        population: planet.population,
        rotation: planet.rotation_period,
        diameter: planet.diameter
      });
    });
  }

  render() {
    const { title, population, rotation, diameter } = this.state;
    return (
      <div className="jumbotron jumbotron-fluid mb-4">
        <div className="container">
          <div className="row">
            <div className="col-4">
              <img src="" alt="" />
            </div>
            <div className="col-8">
              <h4 className="title">{title}</h4>
              <ul className="list-group mb-4">
                <li className="list-group-item">
                  <span className="text-capitalize">population</span>
                  <span className="ml-2">{population}</span>
                </li>
                <li className="list-group-item">
                  <span className="text-capitalize">rotation period</span>
                  <span className="ml-2">{rotation}</span>
                </li>
                <li className="list-group-item">
                  <span className="text-capitalize">diameter</span>
                  <span className="ml-2">{diameter}</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
