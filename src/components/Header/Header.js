import React from 'react';

export const Header = () => {
  return (
    <ul className="nav nav-pills mb-4 mt-4">
      <li className="nav-item">
        <a className="nav-link text-capitalize active" href="/">
          active
        </a>
      </li>
      <li className="nav-item">
        <a className="nav-link text-capitalize" href="/people">
          people
        </a>
      </li>
      <li className="nav-item">
        <a className="nav-link text-capitalize" href="/planets">
          planets
        </a>
      </li>
      <li className="nav-item">
        <a className="nav-link text-capitalize" href="/startships">
          startships
        </a>
      </li>
    </ul>
  );
};
